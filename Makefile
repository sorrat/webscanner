SHELL := /bin/bash


deps:
	pip install -r requirements.txt

test:
	PYTHONPATH=webscan pytest -x


.PHONY: deps test
