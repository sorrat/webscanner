# -*- coding: utf-8 -*-
"""
Rating of domain based on Alexa Rank
"""
import re

from errors import WebscanError
from utils import between, get_dict_val
from browser import open_page


description = "Alexa rank of domain [http://www.alexa.com]"

ALEXA_RANK_RE = re.compile(r'POPULARITY.*TEXT="(\d+)"')
RATINGS = {
    between('1', '9,999'): 10,
    between('10,000', '49,999'): 7,
    between('50,000', '99,999'): 6,
    between('100,000', '499,999'): 5,
    between('500,000', 'inf'): 4,
}


def extract_alexa_rank(xml):
    try:
        return ALEXA_RANK_RE.findall(xml)[0]
    except IndexError:
        return None


def get_rating(url, browser):
    resp = open_page('http://data.alexa.com/data?cli=10&url=%s' % url)
    rank = extract_alexa_rank(resp.content)
    if rank is None:
        return 0
    rating = get_dict_val(RATINGS, rank)
    if rating is None:
        raise WebscanError('Alexa rank is %r' % rank)
    return rating
