# -*- coding: utf-8 -*-
from __future__ import absolute_import
from urlparse import urljoin
from urllib import urlencode

from lxml import etree

from errors import RequestError
from utils import get_domain, find_elems, contains, substr, solve_captcha


description = 'Checks if the website has a Facebook page or not'


def get_captcha_url(resp):
    relative_url = substr(resp.content, '<img src="', '"').replace('&amp;', '&')
    return urljoin(resp.url, relative_url)


def bypass_google_captcha(resp, browser):
    captcha_url = get_captcha_url(resp)
    captcha_image = browser.get(captcha_url).content
    captcha_text = solve_captcha(captcha_image)
    captcha_id = substr(captcha_url, 'id=', '&')
    query_params = {
        'id': captcha_id,
        'captcha': captcha_text,
        'submit': 'Submit',
    }
    request_url = resp.url.replace('IndexRedirect', 'CaptchaRedirect')
    return browser.get('%s&%s' % (request_url, urlencode(query_params)))


def google_search_old(query_string, browser):
    url = 'https://google.com/search'
    resp = browser.get(url, params={'q': query_string})
    for _ in range(10):
        if resp.status_code == 503:
            resp = bypass_google_captcha(resp, browser)
        else:
            break

    if resp.status_code == 200:
        return map(etree.tostring, find_elems(resp.content, 'div.rc'))
    elif resp.status_code == 503:
        err = ("Can't solve captcha", url)
    else:
        err = ('HTTP %s' % resp.status_code, url)
    raise RequestError(*err)


def google_search(query_string, browser):
    url = 'https://www.google.com/search?q=%s' % query_string
    resp = browser.get(url, use_proxy=True)

    if resp.status_code == 200:
        return map(etree.tostring, find_elems(resp.content, 'div.rc'))
    else:
        err = ('HTTP %s' % resp.status_code, url)
    raise RequestError(*err)


def get_rating(url, browser):
    domain = get_domain(url)
    query_template = 'site:facebook.com+inurl:info+%s'
    search_results = google_search(query_template % domain, browser)
    return 1 if contains(search_results, domain) else 0
