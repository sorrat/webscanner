# -*- coding: utf-8 -*-
from collections import OrderedDict

from . import (
    parked_domain,
    alexa,
    facebook,
    shop,
    registrar,
    language,
)

PLUGINS = OrderedDict((
    ('not_parked', parked_domain),
    ('alexa', alexa),
    ('facebook', facebook),
    ('shop', shop),
    ('registrar', registrar),
    ('language', language),
))
