import logging as log

from utils import get_domain

from pythonwhois.net import get_whois_raw
from pythonwhois.parse import parse_raw_whois


description = 'Check if a domain is registered at certain registrars via who-is'


#  <ext>: [<domain>, <text to match for available domain>]
WHOIS_SERVERS = {
    '.ac.ug': ['wawa.eahd.or.ug', 'no entries found'],
    '.be': ['whois.ripe.net', 'no entries'],
    '.biz': ['whois.biz', 'not found'],
    '.ca': ['whois.cira.ca', 'avail'],
    '.cc': ['whois.nic.cc', 'no match'],
    '.co.ug': ['wawa.eahd.or.ug', 'no entries found'],
    '.co.uk': ['whois.nic.uk', 'no match'],
    '.com': ['com.whois-servers.net', 'no match for'],
    '.com.au': ['whois-check.ausregistry.net.au', 'available\n'],
    '.com.cn': ['whois.cnnic.cn', 'no matching record'],
    '.com.my': ['whois.mynic.net.my', 'does not exist in database'],
    '.com.sg': ['whois.nic.net.sg', 'domain not found'],
    '.com.tw': ['whois.twnic.net', 'no such domain name'],
    '.edu.my': ['whois.mynic.net.my', 'does not exist in database'],
    '.eu': ['whois.eu', 'status:    available'],
    '.in': ['whois.inregistry.net', 'not found'],
    '.info': ['whois.afilias.net', 'not found'],
    '.mobi': ['whois.dotmobiregistry.net', 'not found'],
    '.my': ['whois.mynic.net.my', 'does not exist in database'],
    '.name': ['whois.nic.name', 'no match'],
    '.ne.ug': ['wawa.eahd.or.ug', 'no entries found'],
    '.net': ['whois.crsnic.net', 'no match for'],
    '.net.au': ['whois-check.ausregistry.net.au', 'available\n'],
    '.net.cn': ['whois.cnnic.cn', 'no matching record'],
    '.net.my': ['whois.mynic.net.my', 'does not exist in database'],
    '.net.tw': ['whois.twnic.net', 'no such domain name'],
    '.nl': ['whois.domain-registry.nl', ['is free', 'not a registered domain']],
    '.no': ['whois.norid.no', 'no matches'],
    '.nu': ['whois.nic.nu', 'no match for'],
    '.or.ug': ['wawa.eahd.or.ug', 'no entries found'],
    '.org': ['whois.publicinterestregistry.net', 'not found'],
    '.org.cn': ['whois.cnnic.cn', 'no matching record'],
    '.org.my': ['whois.mynic.net.my', 'does not exist in database'],
    '.org.sg': ['whois.nic.net.sg', 'domain not found'],
    '.org.tw': ['whois.twnic.net', 'no such domain name'],
    '.org.uk': ['whois.nic.uk', 'no match'],
    '.per.sg': ['whois.nic.net.sg', 'domain not found'],
    '.pl': ['whois.dns.pl', 'no information about'],
    '.pt': ['whois.dns.pt', 'no match'],
    '.ro': ['whois.rotld.ro', 'no entries found for the selected'],
    '.sc.ug': ['wawa.eahd.or.ug', 'no entries found'],
    '.se': ['whois.iis.se', 'not found'],
    '.se': ['whois.nic-se.se', 'no data found'],
    '.sg': ['whois.nic.net.sg', 'domain not found'],
    '.tv': ['whois.nic.tv', 'no match for'],
    '.us': ['whois.nic.us', 'not found'],
    '.ws': ['whois.website.ws', 'no match'],
}


def get_possible_extensions(url):
    parts = []
    while True:
        if '.' not in url:
            break
        url = url.split('.', 1)[-1]
        parts.append('.' + url)
    return parts


def select_whois_server(url, whois_servers=WHOIS_SERVERS):
    exts = get_possible_extensions(url)
    exts = sorted(exts, key=len, reverse=True)  # longest first
    for ext in exts:
        if ext in whois_servers:
            return whois_servers[ext][0]
    return None


def whois(domain, server=None):
    raw_data, server_list = get_whois_raw(
        domain=domain,
        server=server or '',
        with_server_list=True,
    )
    parsed_data = parse_raw_whois(
        raw_data=raw_data,
        normalized=[],
        never_query_handles=False,
        handle_server=server_list[-1],
    )
    return parsed_data


def get_rating(url, browser):
    domain = get_domain(url)
    server = select_whois_server(domain)
    log.info('[whois] Selected default WHO-IS server: %s', server or '-')
    info = whois(domain, server)
    registrar = info.get('registrar')
    if registrar:
        log.info('[whois] Registrar of %r: %s', domain, registrar)
        return 1
    return 0
