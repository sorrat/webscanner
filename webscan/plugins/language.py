import re
import logging as log

import detectlanguage

from browser import open_page_with_frames
from plugins.parked_domain.main import clean_html


description = 'Check website language'

LANGUAGE_RATINGS = {
    'en': 3,
    'de': 2,
    'es': 1,
}


def get_html_encoding(page, default='utf-8'):
    """
    Two variants:
    - <meta charset="utf-8"/>
    - <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    """
    matches = re.findall(r'<meta .*?charset="?(\S+?)"', page)
    return matches[0] if matches else default


def decode_html_to_utf8(page):
    enc = get_html_encoding(page)
    return page.decode(enc, 'ignore')


def extract_text_from_html(page, url):
    page = decode_html_to_utf8(page)
    text = clean_html(page, url)
    return text


def detect_language(text):
    text = text[:200].lower()
    return detectlanguage.simple_detect(text)


def get_rating(url, browser):
    page = open_page_with_frames(url).content.lower()
    text = extract_text_from_html(page, url)
    lang = detect_language(text)
    log.info('[language] %s: %s', url, lang)
    rating = LANGUAGE_RATINGS.get(lang, 0)
    return rating
