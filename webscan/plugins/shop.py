# -*- coding: utf-8 -*-
import re
import logging as log

from browser import open_page_with_frames
from utils import between, get_dict_val


description = 'Checks if site selling something'


def splitlines(text):
    return filter(bool, (s.strip().lower() for s in text.splitlines()))


def join_to_regex(patterns):
    return re.compile("(%s)" % "|".join(patterns))


SHOP_TEXT_MARKERS = join_to_regex(splitlines("""
    where to buy
    webshop
    special offers
    payment methods
    shipping
    order now
    buy it
    clearance
    tienda
    comprar
    pagar
    our shop
    shopping cart
    online store
    checkout
    sale
    visa
    mastercard
    cheque
    cash
    interac
    paypal

    accept.*? card
    cards.(?:png|jpg|gif)
"""))

RATINGS = {
    between('0', '0'): 0,
    between('1', '2'): 3,
    between('3', 'inf'): 7,
}


def get_rating(url, browser):
    page = open_page_with_frames(url).content.lower()
    matches = set(SHOP_TEXT_MARKERS.findall(page))
    log.info('[shop] Found %d shop markers in %s: %s', len(matches), url, ', '.join(matches))
    rating = get_dict_val(RATINGS, len(matches))
    return rating
