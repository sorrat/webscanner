# -*- coding: utf-8 -*-
import socket
import re
from functools import partial

from errors import StopCalculation, RequestError
from browser import open_page, open_page_with_frames
from utils import get_domain, different_domains, is_url_bad
from .ipranges import PARKED_IP_RANGES
from .text_markers import TEXT_MARKERS


description = 'Check is domain parked'
# factor = 1


def query_ip(url):
    try:
        return socket.gethostbyname(get_domain(url))
    except socket.error:
        raise StopCalculation("Domain is invalid")


def is_site_real(url):
    resp = open_page(url)
    # open('/home/sorrat/dev/webscan/html/%s.html' % url, 'w').write(resp.content)
    if resp.status_code in (403, 404, 503):
        raise StopCalculation("HTTP %s" % resp.status_code)

    if is_url_bad(resp.url):
        raise StopCalculation("Redirected to bad domain: %s" % resp.url)
    # elif different_domains(url, resp.url):
    #     raise StopCalculation("Redirected to another domain %s" % resp.url)


def check_ip(url, parked_ip_ranges):
    ip = query_ip(url)
    # print 'IP of %r: %r' % (url, ip)
    for provider, ip_range in parked_ip_ranges.items():
        if ip in ip_range:
            raise StopCalculation('IP is owned by %s' % provider)


def check_text(url, text_markers):
    page = open_page_with_frames(url).content.lower()
    for provider, strings in text_markers.items():
        for s in strings:
            if s in page:
                error = "Sign of %r substring '%s' found in page" % (provider, s)
                raise StopCalculation(error)


### MATCH ROOT AND RANDOM PATH

IN_CONSTRUCTION_RE = re.compile('|'.join((
    'launch day',
    'currently under construction',
)))
FLAGS = re.DOTALL | re.MULTILINE | re.IGNORECASE
TAGS_RE = {
    'body': re.compile(r'(?:</head>|<body.*?>)(.*)', FLAGS),
    'head': re.compile(r'<head.*?>(.*)</head', FLAGS),
}

def tag_content(tag, string):
    try:
        return TAGS_RE[tag].findall(string)[0].strip()
    except IndexError:
        return None


def tags_content_equal(tag, page1, page2):
    content1 = tag_content(tag, page1)
    content2 = tag_content(tag, page2)
    if content1 is not None and content2 is not None:
        return content1 == content2
    else:
        return page1 == page2


def pages_matches(page1, page2):
    return (tags_content_equal('head', page1, page2) or
            tags_content_equal('body', page1, page2))


# TODO: compare page parts
def check_path(url):
    resp1 = open_page_with_frames(url)
    random_url = url + '/random/page'
    try:
        resp2 = open_page_with_frames(random_url)
    except RequestError:
        # nothing can say about, just skip to next check
        return

    error = ""
    urls = (resp1.url, resp2.url)
    if different_domains(resp1.url, resp2.url):
        error = 'Domains are different: %r, %r' % urls
    elif resp1.url != resp2.url and pages_matches(resp1.content, resp2.content):
        error = 'The contents of pages %r, %r matches' % urls
    elif resp1.url == resp2.url or resp2.status_code in (404, 403, 503):
        match = IN_CONSTRUCTION_RE.findall(resp1.content)
        if match:
            error = 'Site in construction: found substring %r' % match[0]
    if error:
        raise StopCalculation(error)


### CHECK 'BODY' TAG

def remove_tags(string):
    return re.sub('<.*?>', '', string, flags=re.DOTALL).strip()


def compact_tags(string):
    return re.sub(r'</?\s*(\w+).*?>', r'\1 ', string, flags=re.DOTALL).strip()


def remove_noise(string):
    patterns = (
        r'<(script|style).*?</\1>',
        # r'</?(?:p|b|br|i|tr|td|font|body|html).*?>',
        r'<!--.*?-->',  # comments
        r'</?.*?>',     # all tags
        r'<meta.*?name="keywords".*?/>',
        r'(?:href|src)=".*?"',
        r'&(?:nbsp|amp|lt|gt|copy|160|60|62|38|34|39|169);',
        r'\s',
        r'\s{2,}',
    )
    for pattern in patterns:
        string = re.sub(pattern, ' ', string, flags=(re.I | re.DOTALL))
    return string.strip()


def split_by_sentences(string):
    """
    http://stackoverflow.com/a/25736082
    """
    return re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?|!)\s', string)


def remove_duplicate_sentences(string):
    return ' '.join(set(split_by_sentences(string)))


def clean_html(string, domain):
    re_url = r'\S*%s\S*' % re.escape(domain.lstrip('www.'))
    string = re.sub(re_url, '', string)
    string = remove_noise(string)
    string = compact_tags(string)
    string = remove_duplicate_sentences(string)
    return string


def check_body(url):
    page = open_page_with_frames(url).content
    body = tag_content('body', page)
    if not body:
        raise StopCalculation('Empty body')

    if re.search(r'embed.*?src=["\'].*?\.swf["\']', body, FLAGS):
        # there is embeded flash content, skip to next check
        return

    domain = url.rstrip('/').split('//')[-1]
    cleaned_body = clean_html(body, domain)
    if len(cleaned_body) < 110:
        raise StopCalculation('Small body length: %s' % len(cleaned_body))

    body_text = remove_tags(body)
    # If output of a website is short
    # and it includes domain name itself, then it is parked
    if domain in body_text and body_text.count(' ') < 5:
        raise StopCalculation('Page text is website url')


### MAIN

CHECKS = (
    is_site_real,
    partial(check_text, text_markers=TEXT_MARKERS),
    check_path,
    partial(check_ip, parked_ip_ranges=PARKED_IP_RANGES),
    check_body,
)

def get_rating(url, browser):
    for check in CHECKS:
        try:
            check(url)
        except RequestError as error:
            raise StopCalculation("Can't open %r" % error.url)
    return 1
