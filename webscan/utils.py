# -*- coding: utf-8 -*-
import urlparse
import functools
import uuid

import pyquery
import redis
from antigate import AntiGate

import settings


def delete_file_content(filename):
    open(filename, 'w').close()


def read_csv(path, delimiter=','):
    for line in read_lines(path):
        yield line.split(delimiter)


def select_items(dct, keys):
    return [(k, v) for k, v in dct.items() if k in keys]


def get_domain(url):
    return urlparse.urlparse(url).netloc if '://' in url else url


def different_domains(u1, u2):
    u1, u2 = map(get_domain, (u1, u2))
    return u1 not in u2 and u2 not in u1


def to_float(string):
    return float(string.replace(',', ''))


def between(lo, hi):
    lo = to_float(lo)
    hi = to_float(hi)
    return lambda x: lo <= float(x) <= hi


def get_uid():
    return uuid.uuid4().hex


def get_dict_val(predicate_dict, key):
    for predicate, value in predicate_dict.items():
        if predicate(key):
            return value
    return None


def read_lines(filename):
    for line in open(filename):
        l = line.strip().strip(',')
        if l:
            yield l


def chunks(lst, n):
    """ Yield successive n-sized chunks from 'lst'.
    From http://stackoverflow.com/a/312464
    """
    for i in xrange(0, len(lst), n):
        yield lst[i:i+n]


def memoize(func):
    """
    https://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
    """
    cache = func.cache = {}

    @functools.wraps(func)
    def memoizer(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = func(*args, **kwargs)
        return cache[key]
    return memoizer


def memoize_url(f):
    cached_f = memoize(f)

    def wrapped(url, *args, **kwargs):
        url = url_with_schema(url).strip('/')
        return cached_f(url, *args, **kwargs)
    return wrapped


def url_with_schema(domain):
    if '://' not in domain:
        return 'http://%s' % domain
    return domain


def find_elems(html, selector):
    return pyquery.PyQuery(html)(selector)


def contains(iterable, part):
    for item in iterable:
        if part in item:
            return True
    return False


def tail(string, marker):
    """ Get substring after 'marker' """
    return string.split(marker, 1)[-1]


def substr(string, left_marker, right_marker):
    """ Get a substring between 'left_marker' and 'right_marker' """
    return tail(string, left_marker).split(right_marker, 1)[0]


def solve_captcha(captcha_image):
    return str(AntiGate(settings.ANTIGATE_KEY, captcha_image, binary=True))


def get_redis_connection():
    return redis.StrictRedis(**settings.REDIS_CONF)


### BAD REDIRECTS
bad_urls = read_lines(settings.BAD_REDIRECTS_FILE)
BAD_DOMAINS = set(map(get_domain, bad_urls))

def is_url_bad(url):
    for bad_domain in BAD_DOMAINS:
        if bad_domain.startswith('='):
            if bad_domain.lstrip('=') == url:
                return True
        else:
            if bad_domain in url:
                return True
    return False
