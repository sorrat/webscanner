from Queue import Queue
from threading import Thread
import logging as log

from errors import StopCalculation, RequestError
from browser import open_page, Session
from utils import different_domains, get_domain, is_url_bad, chunks


def is_recently_scanned(domain, history):
    scanned_at = history.get(domain)
    if scanned_at:
        log.info('Skip %s: it has been already scanned at %s', domain, scanned_at)
        return True
    return False


def scan_one(domain, plugins, browsers, history):
    if is_recently_scanned(domain, history):
        return None

    info = {'domain': domain}
    try:
        resp = open_page(domain)
    except RequestError as error:
        info['error'] = 'Can not open the url: %s' % error
        return info

    real_domain = get_domain(resp.url).encode('utf8')
    if is_url_bad(real_domain):
        info['error'] = 'Redirected to bad domain: %s' % real_domain
        return info

    if different_domains(domain, real_domain):
        if is_recently_scanned(real_domain, history):
            return None
        info['redirected_to'] = real_domain

    error = ""
    ratings = {}
    for name, plugin in plugins:
        try:
            ratings[name] = plugin.get_rating(real_domain, browsers[name])
            error = ""
        except StopCalculation as error:
            log.error("Stop '%s'. %s", real_domain, error)
            ratings[name] = error.rating
            break
        except RequestError as error:
            log.error("[%s] Can't open %r: %s", name, error.url, error)
        except Exception:
            log.exception('[%s] Unpredicted error for %r:', name, domain)
    info.update(ratings, total=sum(ratings.values()), error=error)
    return info


def scan_several(domains, plugins, history, result_queue):
    # init browser for each plugin
    browsers = {plugin[0]: Session() for plugin in plugins}
    for domain in domains:
        result = scan_one(domain, plugins, browsers, history)
        if result is not None:
            result_queue.put(result)
    result_queue.put(None)


def scan_all(all_domains, plugins, history, concurrency):
    result_queue = Queue()
    not_finished = 0
    workers_num = max(1, len(all_domains) // concurrency)

    for domains in chunks(all_domains, n=workers_num):
        worker = Thread(target=scan_several,
                        args=(domains, plugins, history, result_queue))
        worker.start()
        not_finished += 1

    while not_finished:
        rating = result_queue.get()
        if rating is None:
            not_finished -= 1
            continue
        yield rating
