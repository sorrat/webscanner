# -*- coding: utf-8 -*-
import logging
import logging.config
import os
from os.path import dirname, join, abspath

import detectlanguage
from envparse import env


def mkdir(path):
    if not os.path.isdir(path):
        os.makedirs(path, exist_ok=True)


def root(*path):
    return os.path.join(ROOT_DIR, *path)


env.read_envfile()

detectlanguage.configuration.api_key = env('DETECT_LANGUAGE_API_KEY')

APP_DIR = dirname(abspath(__file__)) # this dir
ROOT_DIR = dirname(APP_DIR)
VAR_DIR = root('var')
LOG_DIR = root('var/log')
mkdir(LOG_DIR)

DEBUG = env.bool('DEBUG', default=False)
DEFAULT_CONCURRENCY = env.int('CONCURRENCY', default=20)
DEFAULT_OUTFILE = env('OUTFILE', default='ratings.csv')
DEFAULT_POSITIVE_OUTFILE = env('POSITIVE_OUTFILE', default='ratings-positive.csv')
DEFAULT_MIN_POSITIVE_RATING = env.int('MIN_POSITIVE_RATING', default=1)
DEFAULT_HISTORY_FILE = env('HISTORY_FILE', default='history.csv')
DEFAULT_HISTORY_AGE = env.int('HISTORY_AGE', default=30)

BAD_REDIRECTS_FILE = join(APP_DIR, 'bad-redirects.txt')
LOCK_PATH = join(VAR_DIR, '.lock')

ANTIGATE_KEY = env('ANTIGATE_KEY')
USERAGENTS_FILE = join(VAR_DIR, 'useragents.txt')

PROXIES_FILE = join(VAR_DIR, 'proxies.txt')
PROXY_BLOCK_DURATION = env.int('PROXY_BLOCK_DURATION', default=60 * 60)  # 1 hour
# interval between proxy's requests
PROXY_PAUSE_DURATION = env.int('PROXY_PAUSE_DURATION', default=60)
MAX_REQUEST_ATTEMPTS = env.int('MAX_REQUEST_ATTEMPTS', default=5)
PROXY_LOG_FILE = env('PROXY_LOG_FILE', default=root('var/proxy-log.csv'))

REDIS_CONF = {
    'host': '127.0.0.1',
    'port': 6379,
    'db': 0,
}

# --- LOGGING

def rotating_file_handler(filename, loglevel):
    return {
        'level': loglevel,
        'class': 'logging.handlers.RotatingFileHandler',
        'filename': os.path.join(LOG_DIR, filename),
        'maxBytes': 1024 * 1024 * 5,  # 5 MB
        'backupCount': 5,
        'formatter': 'simple',
    }

def logger(level):
    return {
        'handlers': ['console'] if DEBUG else ['file'],
        'level': level,
        'propagate': False,
    }


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(asctime)s [%(levelname).5s] [%(name)s] %(message)s',
            'datefmt': '%d-%b %H:%M:%S',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
        },
        'file': rotating_file_handler('app.log', 'DEBUG'),
    },
    'loggers': {
        '': logger('DEBUG'),
        'filelock': logger('WARNING'),
        'urllib3': logger('INFO'),
    }
}
logging.config.dictConfig(LOGGING)
