# -*- coding: utf-8 -*-
"""
Replacement for 'requests.get'
"""
import socket
import tempfile
from urllib import urlencode
from cStringIO import StringIO

import pycurl

from errors import RequestError
from utils import memoize_url


DEFAULT_USERAGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0'


class Response(object):
    def __init__(self, url, content, status_code):
        self.url = url
        self.content = content
        self.status_code = status_code


def init_curl(url, query=None, data=None, follow=True, timeout=30):
    c = pycurl.Curl()
    if query:
        url += '?%s' % urlencode(query)
    c.setopt(pycurl.URL, url)
    c.setopt(pycurl.FOLLOWLOCATION, follow)
    c.setopt(pycurl.ENCODING, "gzip, deflate")
    c.setopt(pycurl.MAXREDIRS, 10)
    c.setopt(pycurl.CONNECTTIMEOUT, 20)
    c.setopt(pycurl.TIMEOUT, 300)
    c.setopt(pycurl.NOSIGNAL, True)
    c.setopt(pycurl.USERAGENT, DEFAULT_USERAGENT)
    # disable ssl verification
    c.setopt(pycurl.SSL_VERIFYPEER, False)
    c.setopt(pycurl.SSL_VERIFYHOST, False)
    # enable cookie support
    c.cookiefile = tempfile.NamedTemporaryFile()
    c.setopt(pycurl.COOKIEFILE, c.cookiefile.name)
    c.setopt(pycurl.COOKIEJAR, c.cookiefile.name)
    # save response's content
    c.body = StringIO()
    c.setopt(c.WRITEDATA, c.body)
    if data:
        # add post-data
        c.setopt(pycurl.POSTFIELDS, urlencode(data))
    return c


@memoize_url
def open_page(url, query=None, data=None, follow=True, timeout=30):
    c = init_curl(url, query, data, follow, timeout=timeout)
    try:
        c.perform()
    except (socket.error, pycurl.error) as error:
        raise RequestError(error, url)
    else:
        return Response(url=c.getinfo(pycurl.EFFECTIVE_URL),
                        content=c.body.getvalue(),
                        status_code=c.getinfo(pycurl.HTTP_CODE))
    finally:
        c.close()
        c.cookiefile.close()
