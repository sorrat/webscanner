# -*- coding: utf-8 -*-
from __future__ import absolute_import
import socket
import random
import time
import logging as log

import requests

import settings
from lib import CsvLogger
from errors import RequestError
from utils import memoize_url, read_lines
from .proxy_manager import ProxyManager
from . import reqdump


# https://techblog.willshouse.com/2012/01/03/most-common-user-agents/
# TODO: periodically update file
USER_AGENTS = tuple(read_lines(settings.USERAGENTS_FILE))


@memoize_url
def open_page(url):
    return Session().get(url)


BAD_PROXY_MARKERS = [
    "we're sorry",
]


def is_response_bad(resp, error):
    if resp is None:
        return str(error)

    if not resp.ok:
        return 'HTTP %s' % resp.status_code

    resp_text = resp.text
    for bad_marker in BAD_PROXY_MARKERS:
        if bad_marker in resp_text:
            return resp_text
    return None


def random_user_agent():
    # random choice from top-15 most frequent user-agents
    return random.choice(USER_AGENTS[:15])


class Session(object):
    def __init__(self):
        self.session = None
        self.reset()

    def reset(self):
        self.session = self._factory()

    def _factory(self):
        session = requests.Session()
        session.headers.update({
            'user-agent': random_user_agent(),
            'accept-language': 'en-US,en;q=0.8,ru;q=0.6,pl;q=0.4',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'upgrade-insecure-requests': '1',
        })
        return session

    def request(self, *args, **kwargs):
        if settings.DEBUG:
            # dump to stdout HTTP communication
            kwargs['hooks'] = {'response': reqdump.dump}

        use_proxy = kwargs.pop('use_proxy', False)
        _request = request_through_proxy if use_proxy else request
        return _request(self.session, *args, **kwargs)

    def get(self, url, **kwargs):
        return self.request('get', url=url, **kwargs)

    def post(self, url, data, **kwargs):
        return self.request('post', url=url, data=data, **kwargs)


def request(session, method, url, **kwargs):
    kwargs.setdefault('timeout', 20)
    try:
        return session.request(method, url, **kwargs)
    except (requests.RequestException, socket.error) as error:
        raise RequestError(error, url)


def get_proxy_logger():
    path = settings.PROXY_LOG_FILE
    columns = [
        'timestamp',
        'proxy',
        'query',
        'success',
        'error_message',
        'user_agent',
    ]
    return CsvLogger(path, columns, '|')


def save_proxy_result(logger, proxy, user_agent, url, error_msg):
    timestamp = round(time.time(), 2)
    query = url.split('?')[-1].replace('q=', '')
    success = int(not bool(error_msg))

    result = dict(
        timestamp=timestamp,
        proxy=proxy,
        query=query,
        success=success,
        error_message=error_msg,
        user_agent=user_agent,
    )
    logger.write(result)


def request_through_proxy(session, method, url, **kwargs):
    error = ''
    manager = ProxyManager()
    lives = settings.MAX_REQUEST_ATTEMPTS
    logger = get_proxy_logger()

    while lives > 0:
        lives -= 1
        proxy = manager.get_free_proxy()
        if proxy:
            log.info('Selected proxy: %s', proxy)
            manager.pause_proxy(proxy)
            proxy_uri = 'socks5://%s' % proxy
            kwargs.setdefault('proxies', {'http': proxy_uri,
                                          'https': proxy_uri})
        else:
            log.warning('Working without proxy: no free proxy left.')
            kwargs['proxies'] = None
            lives = 0

        user_agent = random_user_agent()
        kwargs.setdefault('headers', {})['user-agent'] = user_agent
        kwargs.setdefault('timeout', 20)
        try:
            resp = session.request(method, url, **kwargs)
        except (requests.RequestException, socket.error) as e:
            resp = None
            error = e

        error_msg = is_response_bad(resp, error)
        if error_msg is None:
            save_proxy_result(logger, proxy, user_agent, url, '')
            break
        if proxy:
            manager.block_proxy(proxy)
            save_proxy_result(logger, proxy, user_agent, url, error_msg)
            log.info('Blocked proxy %r due to: %s', proxy, error_msg)

    if resp is None:
        raise RequestError(error, url)
    return resp
