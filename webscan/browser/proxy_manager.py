# -*- coding: utf-8 -*-
from __future__ import absolute_import
import time
import random

import settings
from utils import read_lines, get_redis_connection


PROXY_LIST = set(read_lines(settings.PROXIES_FILE))


class ProxyManager(object):
    keys = {
        'paused': 'ws:proxies:paused',
        'blocked': 'ws:proxies:blocked',
    }

    def __init__(self,
                 pause_duration=settings.PROXY_PAUSE_DURATION,
                 block_duration=settings.PROXY_BLOCK_DURATION):
        self.pause_duration = pause_duration
        self.block_duration = block_duration
        self.redis = get_redis_connection()

    def pause_proxy(self, proxy):
        key = self.keys['paused']
        unpause_time = time.time() + self.pause_duration
        self.redis.zadd(key, unpause_time, proxy)

    def block_proxy(self, proxy):
        key = self.keys['blocked']
        unblock_time = time.time() + self.block_duration
        self.redis.zadd(key, unblock_time, proxy)

    def get_not_expired_members(self, key):
        self.redis.zremrangebyscore(key, 0, time.time())
        return set(self.redis.zrange(key, 0, -1))

    def get_free_proxies(self):
        stopped = set()
        stopped.update(self.get_paused_proxies())
        stopped.update(self.get_blocked_proxies())
        return PROXY_LIST - stopped

    def get_free_proxy(self):
        free_proxies = tuple(self.get_free_proxies())
        if not free_proxies:
            return None
        return random.choice(free_proxies)

    def get_paused_proxies(self):
        key = self.keys['paused']
        return self.get_not_expired_members(key)

    def get_blocked_proxies(self):
        key = self.keys['blocked']
        return self.get_not_expired_members(key)

    # for tests
    def _cleanup(self):
        keys = tuple(self.keys.values())
        self.redis.delete(*keys)
