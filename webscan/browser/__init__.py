# -*- coding: utf-8 -*-
# curl is default browser
from .current_browser import open_page
from .frames import open_page_with_frames
from .requests import Session
