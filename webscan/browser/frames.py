# -*- coding: utf-8 -*-
import re
from urlparse import urljoin

from browser.current_browser import open_page
from utils import memoize_url


def handle_redirect(resp):
    """
    Handle redirects to other pages like
    <meta http-equiv="refresh" content="0;url= Aussie_Clobber/Home.html" />
    """
    page = resp.content
    matches = re.findall(r'http-equiv=["\']refresh["\'] .*?content=["\'].*?url=(.*?)["\']',
                         page, re.DOTALL | re.MULTILINE | re.IGNORECASE)
    if matches:
        refresh_url = urljoin(resp.url, matches[0].strip())
        resp = open_page(refresh_url)
        return handle_redirect(resp)
    else:
        return resp


def open_frames(page, url):
    """Open frames to other pages like '<frame src="rechts.html" ...>'
    and combine them with original page.
    Also handle the same way redirects like 'location.href="..."'
    """
    flags = re.DOTALL | re.MULTILINE | re.IGNORECASE
    matches = re.findall(r'<frame\b.*?src=["\'](.*?)["\']', page, flags)
    matches += re.findall(r'\blocation.href=["\'](.*?)["\']', page, flags)
    frames = [page]
    for match in matches:
        frame_url = urljoin(url, match.strip())
        frame = open_page(frame_url).content
        frames.append(frame)
    return '\r\n'.join(frames)


@memoize_url
def open_page_with_frames(url):
    resp = handle_redirect(open_page(url))
    resp.content = open_frames(resp.content, resp.url)
    return resp
