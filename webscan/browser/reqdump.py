"""
dump HTTP communication invoked by requests

https://github.com/torufurukawa/requests-dump
"""

from sys import stdout


# --------------------------------------------------------------
# Hook function
# --------------------------------------------------------------

def dump(res, *args, **kw):
    """dump HTTP request and response"""
    # request spec
    method = res.request.method
    url = res.url
    _println('> %s %s' % (method, url))

    # request headers
    for k, v in res.request.headers.items():
        _println('> %s: %s' % (k, v))

    # reqeuest body
    body = res.request.body
    if body:
        _println(body)
    _println('')

    # response code
    _println('< %s %s' % (res.status_code, res.reason))

    # response headers
    for k, v in res.headers.items():
        _println('< %s: %s' % (k, v))

    # response body
    _println(res.text)
    _println('')


def truncate(text, limit):
    if len(text) > limit:
        return text[:limit] + '...'
    else:
        return text


def _println(text):
    line = truncate(text, 750)
    try:
        stdout.write(line)
    except TypeError:
        print(line)
    stdout.write('\n')
