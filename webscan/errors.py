# -*- coding: utf-8 -*-

class WebscanError(Exception):
    """General app error"""


class RequestError(WebscanError):
    """Can not open URL"""

    def __init__(self, msg='', url=''):
        super(RequestError, self).__init__(msg)
        self.url = url


class StopCalculation(WebscanError):
    """Stop rating calculation"""

    def __init__(self, msg='', rating=0):
        super(StopCalculation, self).__init__(msg)
        self.rating = rating
