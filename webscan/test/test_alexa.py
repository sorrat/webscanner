# -*- coding: utf-8 -*-
import numbers

import pytest
from mock import patch

from errors import WebscanError
from plugins.alexa import extract_alexa_rank, get_rating, RATINGS


def test_ratings_dict():
    for predicate in RATINGS:
        assert isinstance(predicate(100), numbers.Number)


def test_extract_alexa_rank():
    rank = "9243"
    good = """
    <?xml version="1.0" encoding="UTF-8"?>
    <ALEXA VER="0.9" URL="example.com/" HOME="0" AID="=" IDN="example.com/">
    <SD>
    <POPULARITY URL="example.com/" TEXT="%s" SOURCE="panel"/>
    <REACH RANK="6687"/>
    <RANK DELTA="-292"/>
    <COUNTRY CODE="US" NAME="United States" RANK="8975"/>
    </SD>
    </ALEXA>
    """ % rank
    assert extract_alexa_rank(good) == rank

    bad = """
    <?xml version="1.0" encoding="UTF-8"?>
    <ALEXA VER="0.9" URL="svfdsf.com/" HOME="0" AID="=" IDN="svfdsf.com/">
    </ALEXA>
    """
    assert extract_alexa_rank(bad) is None


@patch('plugins.alexa.get_dict_val')
@patch('plugins.alexa.extract_alexa_rank')
@patch('plugins.alexa.open_page')
def test_rating(open_page, extract_alexa_rank, get_dict_val):
    r = 'some_rating'
    b = None
    get_dict_val.return_value = r
    assert get_rating('http://some_url', b) == r
    assert open_page.called
    assert extract_alexa_rank.called
    assert get_dict_val.called

    get_dict_val.return_value = None
    assert pytest.raises(WebscanError, get_rating, 'http://some_url', b)
