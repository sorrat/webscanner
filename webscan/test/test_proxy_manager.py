# -*- coding: utf-8 -*-
from __future__ import absolute_import

from mock import patch

from browser import proxy_manager as pm


@patch.dict('settings.REDIS_CONF', db=15)
def test_proxy_manager():
    manager = pm.ProxyManager()
    manager._cleanup()

    all_proxies = set(('1', '2'))
    pm.PROXY_LIST = all_proxies

    assert manager.get_free_proxies() == all_proxies

    manager.pause_proxy('1')
    assert manager.get_paused_proxies() == set(('1'))
    assert manager.get_free_proxies() == set(('2'))
    assert manager.get_free_proxy() == '2'

    manager.block_proxy('2')
    assert manager.get_blocked_proxies() == set(('2'))
    assert manager.get_free_proxies() == set()
    assert manager.get_free_proxy() is None
