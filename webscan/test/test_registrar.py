from plugins.registrar import select_whois_server


def test_select_whois_server():
    whois_servers = {
        '.au.com': ['2'],
        '.com': ['1'],
        '.de.com': ['3'],
        '.de.com.au': ['4'],
    }
    for ext, servers in whois_servers.items():
        selected = select_whois_server('mail.ya' + ext, whois_servers)
        expected = servers[0]
        assert selected == expected
