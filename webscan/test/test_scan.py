# -*- coding: utf-8 -*-
from mock import patch

from scanner import scan_one
from errors import StopCalculation
from plugins import PLUGINS
from conftest import init_mocks, init_mock


def test_plugins():
    for plugin_name, plugin in PLUGINS.items():
        assert callable(plugin.get_rating)
        assert isinstance(plugin.description, basestring)


@patch('scanner.open_page')
def test_scan(open_page):
    domain = 'http://some_url_1'
    open_page.return_value.url = domain
    plugins = init_mocks(2)
    args = {
        'domain': domain,
        'plugins': zip(('one', 'two'), plugins),
        'browsers': init_mock(),
        'history': {},
    }

    plugins[0].get_rating.return_value = 10
    plugins[1].get_rating.return_value = 7
    info = scan_one(**args)
    assert info == {
        'domain': domain,
        'one': 10,
        'two': 7,
        'total': 17,
        'error': ''
    }

    plugins[0].get_rating.side_effect = StopCalculation(rating=5)
    info = scan_one(**args)
    assert info['total'] == 5
    assert info['error']
