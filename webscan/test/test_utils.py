# -*- coding: utf-8 -*-

from utils import (
    select_items, get_domain, to_float, between, get_dict_val,
    different_domains, url_with_schema
)


def test_select_items():
    d = {'one': 1, 'two': 2, 'three': 3}
    assert select_items(d, ()) == []
    assert select_items(d, ('one',)) == [('one', 1)]
    assert set(select_items(d, ('one', 'two'))) == set((('one', 1), ('two', 2)))


def test_get_domain():
    url = 'google.com'
    assert get_domain(url) == url
    assert get_domain('http://' + url) == url
    assert get_domain('https://' + url) == url


def test_different_domains():
    assert not different_domains('google.com', 'https://google.com')
    assert different_domains('google.com', 'https://google.ru')
    assert different_domains('google.com', 'https://yahoo.com')


def test_to_num():
    eps = 1e-10
    assert abs(to_float('1') - 1) < eps
    assert abs(to_float('1,1') - 11) < eps


def test_between():
    assert between('1', '100')('50')
    assert between('1', '100')('100')
    assert not between('1', '100')('0')
    assert not between('1', '100')('101')


def test_get_dict_val():
    d = {
        lambda x: x > 10: '>10',
        lambda x: x < 10: '<10',
    }
    assert get_dict_val(d, 5) == '<10'
    assert get_dict_val(d, 20) == '>10'


def test_url_with_schema():
    assert url_with_schema('url') == 'http://url'
    assert url_with_schema('https://url') == 'https://url'
