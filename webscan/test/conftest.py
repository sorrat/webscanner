import sys
from os.path import dirname
sys.path.insert(0, dirname(dirname(__file__)))

from mock import MagicMock


def init_mocks(n):
    return [MagicMock() for _ in range(n)]

def init_mock():
    return MagicMock()
