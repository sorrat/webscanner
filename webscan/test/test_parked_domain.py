# -*- coding: utf-8 -*-
from iptools import IpRangeList
from mock import patch
import pytest

from plugins.parked_domain.ipranges import PARKED_IP_RANGES
from plugins.parked_domain.text_markers import TEXT_MARKERS
from plugins.parked_domain.main import (
    query_ip, check_ip, check_text, check_path, get_rating)
from errors import StopCalculation
from conftest import init_mocks, init_mock


def test_var_parked_ip_ranges():
    assert isinstance(PARKED_IP_RANGES, dict)
    ipranges = PARKED_IP_RANGES.items()[0][1]
    assert isinstance(ipranges, IpRangeList)


def test_var_text_markers():
    assert isinstance(TEXT_MARKERS, dict)
    strings = TEXT_MARKERS.items()[0][1]
    assert isinstance(strings, tuple)


@patch('socket.gethostbyname')
def test_query_ip(gethostbyname):
    query_ip('http://google.com')
    gethostbyname.assert_called_with('google.com')

    query_ip('google.com')
    gethostbyname.assert_called_with('google.com')


@patch('plugins.parked_domain.main.query_ip')
def test_check_ip(query_ip):
    ip_ranges = {
        'provider1': IpRangeList('20.0.0.0/24'),
        'provider2': IpRangeList('10.0.0.0/24'),
    }
    query_ip.return_value = '10.0.0.2'
    with pytest.raises(StopCalculation):
        assert check_ip('url1', ip_ranges)
    assert query_ip.called

    query_ip.return_value = '9.0.0.2'
    assert check_ip('url2', ip_ranges) is None
    assert query_ip.called


@patch('browser.frames.open_page')
def test_check_text(open_page):
    marker = 'sign'
    text_markers = {
        'provider1': ('bla bla', marker),
    }
    open_page.return_value.content = 'vsfnkjsg sfvljs sfvkj'
    assert check_text('url1', text_markers) is None

    open_page.return_value.content = 'vsfnkjsg sf%svljs sfvkj' % marker
    with pytest.raises(StopCalculation):
        check_text('url2', text_markers)


@patch('browser.frames.open_page')
def test_check_path(open_page):
    is_parked = check_path
    m1, m2 = init_mocks(2)

    # same content, different urls -> domain is parked
    m1.content = '11'
    m2.content = '11'
    m1.url = 'url1'
    m2.url = 'url2'
    open_page.side_effect = [m1, m2]
    with pytest.raises(StopCalculation):
        assert is_parked('some_url_1')
    assert open_page.call_count == 2

    # same content, urls redirected to one -> domain isn't parked
    m1.url = 'url1'
    m2.url = 'url1'
    open_page.side_effect = [m1, m2]
    assert not is_parked('some_url_2')

    # different content in different urls -> domain isn't parked
    domain = 'http://some_url_3'
    m1.content = '11'
    m2.content = '22'
    m1.url = '%s/1' % domain
    m2.url = '%s/2' % domain
    open_page.side_effect = [m1, m2]
    assert not is_parked(domain)


@patch('plugins.parked_domain.main.CHECKS')
def test_rating(checks):
    m = init_mocks(3)
    b = init_mock()
    checks.__iter__ = lambda _: iter(m)

    m[0].return_value = None
    m[1].return_value = None
    m[2].return_value = None
    assert get_rating('url', b) == 1
    assert m[0].called
    assert m[1].called
    assert m[2].called

    m[2].side_effect = StopCalculation
    exc_info = pytest.raises(StopCalculation, get_rating, 'url', b)
    assert exc_info.value.rating == 0
