# -*- coding: utf-8 -*-
import os
import sys
import argparse
from collections import OrderedDict
from datetime import datetime, timedelta

import filelock

import settings
from plugins import PLUGINS
from utils import select_items, read_csv, delete_file_content
from scanner import scan_all
from writer import CsvDictWriter, CsvWriter
from browser.proxy_manager import ProxyManager


lock = filelock.FileLock(settings.LOCK_PATH)


def read_history_file(history_file, history_age):
    if not os.path.exists(history_file):
        return {}

    now = datetime.now()
    history = {}
    history_age = timedelta(days=history_age)

    # file's format: <domain>, <scan_date>
    for row in read_csv(history_file):
        domain = row[0]
        scan_date_str = row[1]
        scan_date = datetime.strptime(scan_date_str, '%Y-%m-%d')

        if scan_date + history_age > now:
            # still relevant (not expired)
            history[domain] = scan_date_str
    return history


def scan_and_save(user_data, plugins, concurrency, outfile,
                  positive_outfile, min_positive_rating,
                  history_file, history_age):

    with lock:
        history = read_history_file(history_file, history_age)

    all_domains = user_data.keys()
    ratings = scan_all(all_domains, plugins, history, concurrency)

    if outfile is None:
        # print to stdout and quit
        for rating in ratings: print rating
        return

    # save ratings to file
    plugin_names = [t[0] for t in plugins]
    additional_keys = user_data.values()[0].keys()
    header = (['domain', 'redirected_to']
              + additional_keys
              + plugin_names
              + ['total', 'error'])

    with lock:
        writer = CsvDictWriter(outfile, header)
        writer_positive = CsvDictWriter(positive_outfile, header)
        writer_history = CsvWriter(history_file, mode='w')
        now = datetime.now().strftime('%Y-%m-%d')

        for domain, scan_date in history.items():
            writer_history((domain, scan_date))

        for rating in ratings:
            rating.update(user_data[rating['domain']])  # add user data
            writer(rating)
            if rating.get('total', 0) >= min_positive_rating:
                writer_positive(rating)
            writer_history((rating['domain'], now))


def prioritize_positive_ratings(positive_file):
    writers = {
        'high_priority': CsvWriter('high-priority.csv', mode='a'),
        'low_priority': CsvWriter('low-priority.csv', mode='a'),
        'processed': CsvWriter('processed.csv', mode='a'),
    }
    fieldnames = None
    for row in read_csv(positive_file):
        if row[0] == 'domain':
            fieldnames = row
            writers['processed'](row)

        elif fieldnames:
            rating = OrderedDict(zip(fieldnames, row))

            user_data = [value for key, value in rating.items() if 'user_data' in key]
            domain = rating['redirected_to'] or rating['domain']
            record = [domain] + user_data

            writer = 'high_priority' if int(rating['total']) > 1 else 'low_priority'
            # print '%s: %s' % (writer, record)
            writers[writer](record)
            writers['processed'](row)

        else:
            raise ValueError("[%s] Can't determine fieldnames for row %s" %
                             (positive_file, row))
    delete_file_content(positive_file)


def cmdline_args():
    parser = argparse.ArgumentParser(
        description='Websites scanner',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('domains', metavar='domain', nargs='*',
                        help='Domains to scan')
    parser.add_argument('--list-plugins', action='store_true',
                        help='Show available plugins')
    parser.add_argument('--plugins', default=PLUGINS.keys(),
                        help='Choose plugins to run scanner with')
    parser.add_argument('--concurrency', '-c', type=int,
                        default=settings.DEFAULT_CONCURRENCY,
                        help='Number of concurrent workers')
    parser.add_argument('--urls-file', '-f',
                        help='File with website urls to scan')
    parser.add_argument('--stdout', '-s', action='store_true',
                        help='Print ratings in the terminal')
    parser.add_argument('--outfile', '-o', default=settings.DEFAULT_OUTFILE,
                        help="Save ratings in this file")
    parser.add_argument('--positive-outfile', '-p',
                        default=settings.DEFAULT_POSITIVE_OUTFILE,
                        help="Save positive ratings in this file")
    parser.add_argument('--min-positive-rating', '-m', type=int,
                        default=settings.DEFAULT_MIN_POSITIVE_RATING,
                        help="Min rating to save positive")
    parser.add_argument('--history-file',
                        default=settings.DEFAULT_HISTORY_FILE,
                        help="CSV-file with history of domain's scans")
    parser.add_argument('--history-age',
                        default=settings.DEFAULT_HISTORY_AGE,
                        type=int,
                        help="Number of days to keep domain's scan history")
    parser.add_argument('--list-blocked-proxies',
                        action='store_true',
                        help='Print a list of currently blocked proxies')
    parser.add_argument('--prioritize-positive-ratings',
                        action='store_true',
                        help='Divide scanned domains with positive rating '
                             'in two categories: high-priority, low-priority')
    return parser, parser.parse_args()


if __name__ == '__main__':
    parser, args = cmdline_args()
    if args.list_plugins:
        for name, plugin in PLUGINS.items():
            print '%s: %s' % (name, plugin.description)
        sys.exit()

    if args.prioritize_positive_ratings:
        prioritize_positive_ratings(args.positive_outfile)
        sys.exit()

    if args.list_blocked_proxies:
        proxies = ProxyManager().get_blocked_proxies()
        print '\n'.join(proxies)
        sys.exit()

    if args.urls_file:
        user_data = {}
        for row in read_csv(args.urls_file):
            (domain, domain_data) = row[0], row[1:]
            user_data[domain] = OrderedDict((
                ('user_data_%s' % i, value)
                for i, value in enumerate(domain_data, start=1)
            ))

    elif args.domains:
        # no user input data except domains
        user_data = {domain: {} for domain in args.domains}
    else:
        parser.error('No domains')

    if args.stdout:
        outfile = None
    elif args.outfile:
        outfile = args.outfile

    plugins = select_items(PLUGINS, args.plugins)
    scan_and_save(user_data, plugins, args.concurrency, outfile,
                  args.positive_outfile, args.min_positive_rating,
                  args.history_file, args.history_age)
