import os
import logging

import filelock

from settings import root
from utils import memoize


lock_path = root('var/.lock_log')
lock = filelock.FileLock(lock_path)


@memoize
class CsvLogger(object):
    def __init__(self, path, columns, delimiter=','):
        self.columns = columns
        self.logger = self._get_logger(path, self.columns, delimiter)

        # write header as first line if file is new
        with lock:
            if not os.path.getsize(path):
                with open(path, 'a') as fp:
                    header = delimiter.join(self.columns)
                    fp.write(header + '\n')

    def _get_logger(self, path, columns, delimiter):
        name = path.split('/')[-1]
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)

        format_string = delimiter.join('%%(%s)s' % k for k in columns)
        formatter = logging.Formatter(format_string)

        ch = logging.FileHandler(path)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        logger.propagate = False
        return logger

    def write(self, data, level='debug'):
        write_ = getattr(self.logger, level.lower())
        with lock:
            write_('', extra=data)
