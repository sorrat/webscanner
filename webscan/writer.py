import csv


class PeriodicFlush(object):
    def __init__(self, filename, mode):
        self.fd = open(filename, mode)
        self.counter = 0

    def __call__(self, *args):
        self.write(*args)

        self.counter += 1
        if self.counter % 10 == 0:
            self.fd.flush()

    def write(self, *args):
        pass


class CsvDictWriter(PeriodicFlush):
    def __init__(self, filename, header, delimiter=',', mode='a'):
        super(CsvDictWriter, self).__init__(filename, mode)
        self.writer = csv.DictWriter(self.fd, header, delimiter=delimiter)
        self.empty = True

    def write(self, rowdict):
        if self.empty:
            self.writer.writeheader()
            self.empty = False
        self.writer.writerow(rowdict)


class CsvWriter(PeriodicFlush):
    def __init__(self, filename, delimiter=',', mode='w'):
        super(CsvWriter, self).__init__(filename, mode)
        self.writer = csv.writer(self.fd, delimiter=delimiter)

    def write(self, row):
        self.writer.writerow(row)
