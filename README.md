### About ###
It's a website scanner to automatically determine the status of a website (i.e. e-commerce site,
informational site, actively maintained, actively promoted, active but not promoted, abandoned,
parked, empty etc.) and assign the website a score from 1 (blank page) to 100 (a popular and
actively promoted e-commerce site) so that marketing teams have a clear priority list as to which
websites to contact first for the best results.
